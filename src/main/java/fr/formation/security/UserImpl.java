package fr.formation.security;

import fr.formation.model.User;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class UserImpl implements org.springframework.security.core.userdetails.UserDetails {
    private final User usr;

    public UserImpl(User usr) {
        if (usr == null) {
            throw new NullPointerException("L'utilisateur n'existe pas.");
        }
        this.usr = usr;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return usr.getAuthorities();
    }

    @Override
    public String getPassword() {
        return usr.getPassword();
    }

    @Override
    public String getUsername() {
        return usr.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public String getAuthToken() {
        return usr.getToken();
    }
}

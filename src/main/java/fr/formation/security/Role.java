package fr.formation.security;

public enum Role {
    ROLE_USER, ROLE_STAGIAIRE, ROLE_FORMATEUR, ROLE_GESTIONNAIRE;

    @Override
    public String toString() {
        switch (this) {
            case ROLE_USER:
                return "ROLE_USER";
            case ROLE_STAGIAIRE:
                return "ROLE_STAGIAIRE";
            case ROLE_FORMATEUR:
                return "ROLE_ FORMATEUR";
            case ROLE_GESTIONNAIRE:
                return "ROLE_GESTIONNAIRE";
        }
        return null;
    }
}


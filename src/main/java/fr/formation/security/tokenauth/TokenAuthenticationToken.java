package fr.formation.security.tokenauth;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;

public class TokenAuthenticationToken extends AbstractAuthenticationToken implements Principal {
    private final String token;
    private final String userName;

    // Use this constructor for unauthenticated users with tokens
    public TokenAuthenticationToken(String token) {
        super(new ArrayList<>());
        this.token = token;
        this.userName = null;
        setAuthenticated(false);
    }

    // Use this constructor for authenticated users
    public TokenAuthenticationToken(String userName, String token, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.userName = userName;
        this.token = token;
        setAuthenticated(true);
    }

    @Override
    public String getPrincipal() {
        return userName;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}

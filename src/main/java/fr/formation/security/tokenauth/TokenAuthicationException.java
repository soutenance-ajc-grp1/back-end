package fr.formation.security.tokenauth;

import org.springframework.security.core.AuthenticationException;

public class TokenAuthicationException extends AuthenticationException {

    public TokenAuthicationException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public TokenAuthicationException(String msg) {
        super(msg);
    }
}

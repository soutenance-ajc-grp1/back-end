package fr.formation.security.tokenauth;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TokenAuthenticationFilter extends OncePerRequestFilter {

    private AuthenticationManager am;

    public TokenAuthenticationFilter(AuthenticationManager am) {
        this.am = am;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String token = request.getHeader("X-token");
        if (token == null) {
            filterChain.doFilter(request, response);
            return;
        }

        // Authenticate User with a token then continue the filterChain
        TokenAuthenticationToken authentication = new TokenAuthenticationToken(token);
        Authentication authenticatedToken = am.authenticate(authentication);
        SecurityContextHolder.getContext().setAuthentication(authenticatedToken);
        filterChain.doFilter(request, response);
    }
}

package fr.formation.security.tokenauth;

import fr.formation.security.UserImpl;
import fr.formation.service.AuthService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import java.util.Optional;

public class TokenAuthenticationProvider implements AuthenticationProvider {
    private AuthService as;

    public TokenAuthenticationProvider(AuthService as) {
        this.as = as;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        // Cast to TokenAuth Object
        final TokenAuthenticationToken token = (TokenAuthenticationToken) authentication;

        // Find user bearing the same token in DB
        Optional<UserImpl> usr = as.findByToken(String.valueOf(token.getCredentials()));

        // Throw exception if no corresponding user was found
        if (usr.isEmpty()) throw new TokenAuthicationException("No user found matching token.");

        // Return an authenticated token
        return new TokenAuthenticationToken(usr.get().getUsername(), usr.get().getPassword(), usr.get().getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return TokenAuthenticationToken.class.isAssignableFrom(authentication);
    }
}

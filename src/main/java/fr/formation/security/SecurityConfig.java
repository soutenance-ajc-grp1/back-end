package fr.formation.security;

import fr.formation.security.tokenauth.TokenAuthenticationFilter;
import fr.formation.security.tokenauth.TokenAuthenticationProvider;
import fr.formation.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private AuthService as;
    private TokenAuthenticationProvider tokenAuthenticationProvider;

    @Autowired
    public SecurityConfig(AuthService as) {
        this.as = as;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        this.tokenAuthenticationProvider = new TokenAuthenticationProvider(as);
        auth.userDetailsService(as).passwordEncoder(getPasswordEncoder())
                .and().authenticationProvider(tokenAuthenticationProvider);
    }

    @Bean(name = "passwordEncoder")
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }



    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/api/register/**").permitAll()
                .anyRequest().authenticated()
                .and().httpBasic()
                .and().addFilterBefore(new TokenAuthenticationFilter(authenticationManagerBean()), BasicAuthenticationFilter.class);
    }
}

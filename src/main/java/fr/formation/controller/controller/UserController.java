package fr.formation.controller.controller;

import fr.formation.model.Filiere;
import fr.formation.model.User;
import fr.formation.model.dto.UserDTO;
import fr.formation.security.tokenauth.TokenAuthenticationToken;
import fr.formation.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService us;

    @GetMapping("")
    @PreAuthorize("hasRole('GESTIONNAIRE')")
    public List<UserDTO> findAll() {
        return us.findAll().stream().map(UserDTO.DefaultDTO::new).collect(Collectors.toList());
    }

    @GetMapping("/{identifiant}")
    @PreAuthorize("hasRole('GESTIONNAIRE')")
    public UserDTO getById(@PathVariable Integer identifiant) {
        return new UserDTO.DefaultDTO(
                us.getById(identifiant).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "L'utilisateur " + identifiant + " n'existe pas")
                )
        );
    }

    @GetMapping("/me")
    public UserDTO.DefaultDTO getMyInfo(Authentication authentication) {
        return new UserDTO.DefaultDTO(us.findByUsername(authentication.getName()).orElseThrow(() -> new NullPointerException("No user found.")));
    }

    @PostMapping("")
    @PreAuthorize("hasRole('GESTIONNAIRE')")
    @ResponseStatus(HttpStatus.CREATED)
    public void createUtilisateur(@RequestBody User u) {
        us.create(u);
    }


    @PutMapping("")
    @PreAuthorize("hasRole('GESTIONNAIRE')")
    public void updateUtilisateur(@RequestBody User u) {
        us.update(u);
    }

    @PutMapping("/newpassword")
    public void updatePassword(@RequestBody User u, Authentication authentication) {
        TokenAuthenticationToken loggedUsr = (TokenAuthenticationToken) authentication;
        us.updatePassword(u, loggedUsr.getPrincipal());
    }

    @DeleteMapping("/{identifiant}")
    @PreAuthorize("hasRole('GESTIONNAIRE')")
    public void deleteUtilisateur(@PathVariable Integer identifiant) {
        us.delete(identifiant).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "L'utilisateur " + identifiant + " n'existe pas"));
    }

    /*
     * Renvoie la liste des formateurs intervenant sur une filière donné en paramètre
     */
    @GetMapping("/formateurs")
    @PreAuthorize("hasRole('GESTIONNAIRE')")
    public List<UserDTO.DefaultDTO> getFormateursOfFiliere(@RequestParam(name = "filiere_id") Integer id) {
        Filiere f = new Filiere();
        f.setId(id);
        return us.getFormateursOfFiliere(f).stream().map(UserDTO.DefaultDTO::new).collect(Collectors.toList());
    }

}
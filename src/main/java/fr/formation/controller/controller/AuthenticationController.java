package fr.formation.controller.controller;

import fr.formation.model.User;
import fr.formation.model.dto.UserDTO;
import fr.formation.security.Role;
import fr.formation.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("/api/")
public class AuthenticationController {

    private UserService us;

    @Autowired
    public AuthenticationController(UserService us) {
        this.us = us;
    }

    @PostMapping("/register")
    public UserDTO.RegisterDTO register(@RequestBody User u) {
        String pwd = us.randomPwdGenerator(16);
        u.setPassword(pwd);
        u.setRoles(Set.of(Role.ROLE_USER));
        us.create(u);
        return new UserDTO.RegisterDTO(u.getUsername(), pwd);
    }

    @GetMapping("/token")
    public Map<String, String> login(Authentication authentication) throws Exception {
        Map<String, String> response = new HashMap<>();

        User usr = us.findByUsername(authentication.getName()).orElseThrow(() -> new Exception("Logged usr was null."));
        usr.setToken(UUID.randomUUID().toString());
        us.update(usr);

        response.put("token", usr.getToken());
        return response;
    }
}

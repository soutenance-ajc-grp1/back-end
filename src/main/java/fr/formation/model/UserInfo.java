package fr.formation.model;

import javax.persistence.Embeddable;
import java.time.LocalDate;

@Embeddable
public class UserInfo {
    private String mail;
    private String telephone;
    private LocalDate dateNaissance;
    private String adresse;

    public UserInfo() {
    }

    public UserInfo(String mail, String telephone, LocalDate dateNaissance, String adresse) {
        setMail(mail);
        setTelephone(telephone);
        setDateNaissance(dateNaissance);
        setAdresse(adresse);
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
}

package fr.formation.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import fr.formation.model.converters.RoleConverter;
import fr.formation.security.Role;
import fr.formation.security.UserImpl;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "PERSONNE")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    private String token;

    @Column(nullable = false)
    private String nom;

    @Column(nullable = false)
    private String prenom;

    @Embedded
    @JsonUnwrapped
    private UserInfo details;

    @Convert(converter = RoleConverter.class)
    private Set<Role> roles;

    @ManyToOne
    @JoinColumn(name = "filiere_id")
    @JsonIgnoreProperties({"stagiaires", "module"})
    private Filiere filiere;

    @OneToMany(mappedBy = "formateur")
    @JsonIgnoreProperties({"formateur", "filiere"})
    private List<Module> modules;

    public User() {
    }

    public User(Integer id,
                String username,
                String password,
                String token,
                String nom,
                String prenom,
                UserInfo details,
                Set<Role> roles,
                Filiere filiere,
                List<Module> modules) {
        this.details = details;
        setId(id);
        setUsername(username);
        setPassword(password);
        setToken(token);
        setNom(nom);
        setPrenom(prenom);
        setRoles(roles);
        setFiliere(filiere);
        setModules(modules);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    @Transient
    public void setDefaultUsername() {
        this.username = getPrenom().charAt(0) + getNom().toLowerCase();
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom.toUpperCase();
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom.toLowerCase();
    }

    public UserInfo getDetails() {
        return details;
    }

    public void setDetails(UserInfo details) {
        this.details = details;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> role) {
        this.roles = role;
    }

    public Filiere getFiliere() {
        return filiere;
    }

    public void setFiliere(Filiere filiere) {
        this.filiere = filiere;
    }

    public List<Module> getModules() {
        return modules;
    }

    public void setModules(List<Module> modules) {
        this.modules = modules;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", token='" + token + '\'' +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", details=" + details +
                ", roles=" + roles +
                '}';
    }

    @JsonIgnore
    @Transient
    public String getStringRole() {
        return roles.toString();
    }

    @Transient
    public List<GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (Role role : getRoles()) {
            authorities.add(new SimpleGrantedAuthority(role.toString()));
        }
        authorities.add(new SimpleGrantedAuthority(getStringRole()));
        return authorities;
    }

    @Transient
    public UserImpl toUserDetails() {
        return new UserImpl(this);
    }
}

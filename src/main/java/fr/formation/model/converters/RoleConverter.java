package fr.formation.model.converters;

import fr.formation.security.Role;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@Converter
public class RoleConverter implements AttributeConverter<Set<Role>, String> {
    private static final String DELIMITER = ";";

    @Override
    public String convertToDatabaseColumn(Set<Role> roles) {
        return roles.stream().map(Role::toString).collect(Collectors.joining(DELIMITER));
    }

    @Override
    public Set<Role> convertToEntityAttribute(String s) {
        return Arrays.stream(s.split(DELIMITER)).map(Role::valueOf).collect(Collectors.toSet());
    }
}

package fr.formation.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "module")
public class Module {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String libelle;

	private LocalDate dateDebut;

	private LocalDate dateFin;

	@ManyToOne
	@JoinColumn(name = "formateur_id")
	@JsonIgnoreProperties({"modules", "filiere"})
	private User formateur;

	@ManyToOne
	@JoinColumn(name = "filiere_id")
	@JsonIgnoreProperties({"modules", "stagiaire"})
	private Filiere filiere;

	public Module() {

	}

	public Module(Integer id, String libelle, LocalDate dateDebut, LocalDate dateFin, User formateur,
			Filiere filiere) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.formateur = formateur;
		this.filiere = filiere;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}

	public User getFormateur() {
		return formateur;
	}

	public void setFormateur(User formateur) {
		this.formateur = formateur;
	}

	public Filiere getFiliere() {
		return filiere;
	}

	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}

}

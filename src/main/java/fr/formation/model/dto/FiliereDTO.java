package fr.formation.model.dto;

import fr.formation.model.Filiere;
import fr.formation.model.Module;
import fr.formation.model.User;

import java.time.LocalDate;
import java.util.List;

public class FiliereDTO {

	private Integer id;

	private String libelle;

	private List<Module> modules;

	private List<User> stagiaires;

	private LocalDate dateDebut;
	private LocalDate dateFin;

	public FiliereDTO() {

	}

	public FiliereDTO(Integer id, String libelle, List<Module> modules, List<User> stagiaires, LocalDate dateDebut,
			LocalDate dateFin) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.modules = modules;
		this.stagiaires = stagiaires;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
	}

	public FiliereDTO(Integer id, String libelle, List<Module> modules, List<User> stagiaires) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.modules = modules;
		this.stagiaires = stagiaires;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	public List<User> getStagiaires() {
		return stagiaires;
	}

	public void setStagiaires(List<User> stagiaires) {
		this.stagiaires = stagiaires;
	}

	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}

	public static FiliereDTO fromFiliere(Filiere f) {
		return new FiliereDTO(f.getId(), f.getLibelle(), f.getModules(), f.getStagiaires());
	}

}

package fr.formation.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import fr.formation.model.Filiere;
import fr.formation.model.Module;
import fr.formation.model.User;
import fr.formation.security.Role;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public class UserDTO {
    public static class DefaultDTO extends UserDTO {
        public Integer id;
        public String username;
        public String nom;
        public String prenom;
        public Set<Role> roles;
        public String mail;
        public String telephone;
        public LocalDate dateNaissance;
        public String adresse;

        @JsonIgnoreProperties({"stagiaires", "module"})
        public Filiere filiere;

        @JsonIgnoreProperties({"formateur", "filiere"})
        public List<Module> modules;

        public DefaultDTO(User usr) {
            id = usr.getId();
            username = usr.getUsername();
            nom = usr.getNom();
            prenom = usr.getPrenom();
            roles = usr.getRoles();
            filiere = usr.getFiliere();
            modules = usr.getModules();
            mail = usr.getDetails().getMail();
            telephone = usr.getDetails().getTelephone();
            dateNaissance = usr.getDetails().getDateNaissance();
            adresse = usr.getDetails().getAdresse();
        }
    }

    public static class RegisterDTO {
        public String username;
        public String password;

        public RegisterDTO(String username, String password) {
            this.username = username;
            this.password = password;
        }
    }
}

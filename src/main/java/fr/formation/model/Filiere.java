package fr.formation.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "filiere")
public class Filiere {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String libelle;

	@OneToMany(mappedBy = "filiere")
	@JsonIgnoreProperties("filiere")
	private List<Module> modules;

	@OneToMany(mappedBy = "filiere")
	@JsonIgnoreProperties("filiere")
	private List<User> stagiaires;

	public Filiere() {

	}

	public Filiere(Integer id, String libelle, List<Module> modules, List<User> stagiaire) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.modules = modules;
		this.stagiaires = stagiaire;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	public List<User> getStagiaires() {
		return stagiaires;
	}

	public void setStagiaires(List<User> stagiaire) {
		this.stagiaires = stagiaire;
	}

}

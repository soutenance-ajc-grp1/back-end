package fr.formation.repository;

import fr.formation.model.Filiere;
import fr.formation.model.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findUserByUsername(String identifiant);

    @Query(value = "SELECT u FROM User u WHERE u.username LIKE ?1% ORDER BY u.id DESC")
    List<User> findUserBySimilarUsername(@Param("username") String username, Pageable limit);

    @Modifying
    @Query("UPDATE User u SET u.filiere = :f WHERE u.id=:uId AND u.roles='ROLE_STAGIAIRE'")
    @Transactional
    void bindPersonneToFiliere(@Param("uId") Integer userId, @Param("f") Filiere filiere);

    @Query("SELECT u FROM User u JOIN u.modules m WHERE u.roles = 'ROLE_FORMATEUR' AND m.filiere = :f")
    List<User> findFormateursOfFiliere(@Param("f") Filiere f);

    Optional<User> findByToken(String token);

    @Query("SELECT u FROM User u WHERE u.roles NOT LIKE '%ROLE_STAGIAIRE%' AND u.filiere = :f ")
    List<User> findBySouhaitFormation(Filiere f);
}

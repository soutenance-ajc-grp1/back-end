package fr.formation.service;

import fr.formation.model.Filiere;
import fr.formation.model.User;
import fr.formation.repository.UserRepository;
import fr.formation.security.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

@Service
public class UserService {
	
	@Autowired
	UserRepository ur;

	@Autowired
	PasswordEncoder passwordEncoder;
	
	public User create(User u) {
		u.setPassword(passwordEncoder.encode(u.getPassword()));
		u.setRoles(Set.of(Role.ROLE_USER, Role.ROLE_STAGIAIRE));
		u.setDefaultUsername();

		// Find an existing user with the same username to preserve login uniqueness
		Optional<User> sameUsrName = ur.findUserByUsername(u.getUsername());

		// In case same username exists, append a number to it
		if (sameUsrName.isPresent()) {
			try {
				// We use pages to get only the last created user which should have the highest appended number
				Optional<User> lastUsr = ur.findUserBySimilarUsername(u.getUsername(), PageRequest.of(0,1)).stream().findFirst();

				// Extract last used number
				int lastNum = Integer.parseInt(lastUsr.get().getUsername().substring(u.getUsername().length()));

				// Append last used number + 1 to our new user's username
				u.setUsername(u.getUsername() + (lastNum + 1) );
			} catch (Exception e) {

				// Append 1 to username in case of a single duplicate
				u.setUsername(u.getUsername()+"1");
			}
		}
		ur.save(u);
		return u;
	}

	public Optional<User> findByUsername(String username) {
		return ur.findUserByUsername(username);
	}

	public List<User> findAll() {
		return this.ur.findAll();
	}

	public Optional<User> getById(Integer identifiant) {
		return ur.findById(identifiant);
	}

	public void update(User u) {
		ur.save(u);
	}

	public void updatePassword(User u, String userName) {
		User current = ur.findUserByUsername(userName).orElseThrow(() -> new NullPointerException("No user found."));
		current.setPassword(passwordEncoder.encode(u.getPassword()));
		ur.save(current);
	}

	public Optional<Boolean> delete(User u) {
		try {
			this.ur.delete(u);
			return Optional.of(Boolean.TRUE);
		} catch (EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}

	public Optional<Boolean> delete(Integer identifiant) {
		try {
			this.ur.deleteById(identifiant);
			return Optional.of(Boolean.TRUE);
		} catch (EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}

	public List<User> getFormateursOfFiliere(Filiere f) {
		return ur.findFormateursOfFiliere(f);
	}

	public void bindStagiairesToFiliere(List<User> uList, Filiere f) {
		for (User p : uList) {
			ur.bindPersonneToFiliere(p.getId(), f);
		}
	}

	public void bindStagiaireToFiliere(User u, Filiere f) {
		ur.bindPersonneToFiliere(u.getId(), f);
	}

	public Boolean checkAllStagiaire(List<User> uList) {
		for (User u : uList) {
			if (!Role.ROLE_STAGIAIRE.equals(ur.getById(u.getId()).getRoles()))
				return false;
		}
		return true;
	}

	public List<User> findBySouhaitFormation(Filiere f) {
		return ur.findBySouhaitFormation(f);
	}

	public String randomPwdGenerator(int size) {
		String chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		Random rnd = new Random();

		StringBuilder sb = new StringBuilder(size);
		for( int i = 0; i < size; i++) {
			sb.append(chars.charAt(rnd.nextInt(chars.length())));
		}
		return sb.toString();
	}
}

package fr.formation.service;


import fr.formation.model.User;
import fr.formation.repository.UserRepository;
import fr.formation.security.UserImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthService implements UserDetailsService {

    @Autowired
    UserRepository ur;

    @Override
    public UserImpl loadUserByUsername(String identifiant) throws UsernameNotFoundException {
        Optional<User> optU = ur.findUserByUsername(identifiant);

        if (optU.isPresent()) {
            return optU.get().toUserDetails();
        } else {
            throw new UsernameNotFoundException("L'utilisateur " + identifiant + " n'existe pas.");
        }
    }

    public Optional<UserImpl> findByToken(String token) {
        Optional<User> usr = ur.findByToken(token);
        return usr.map(User::toUserDetails);
    }
}
